console.log("Hello");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userDetails(){
		let fullName = prompt("Enter your full name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location: ");

		console.log("Hello " + fullName + "!");
		console.log(fullName + "'s age is " + age);
		console.log(fullName + " is located at " + location);
	};
	userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function musical(){
		let artist = ["GFriend", "IZ*one", "Twice", "Red Velvet", "BlackPink"];
		console.log(artist);
	};
	musical();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function movies(){
		let movie1 = "Series of Unfortunate Events";
		let tomatoMeter1 = "96%";
		let movie2 = "Elona Holmes";
		let tomatoMeter2 = "90%";
		let movie3 = "Titanic";
		let tomatoMeter3 = "91%";
		let movie4 = "Birds of Prey";
		let tomatoMeter4 = "79%";
		let movie5 = "Parasite";
		let tomatoMeter5 = "99%";;

		console.log("1. " + movie1);
		console.log("Tomato Meter for " + movie1 + ": " + tomatoMeter1);
		console.log("2. " + movie2);
		console.log("Tomato Meter for " + movie2 + ": " + tomatoMeter2);
		console.log("3. " + movie3);
		console.log("Tomato Meter for " + movie3 + ": " + tomatoMeter3);
		console.log("4. " + movie4);
		console.log("Tomato Meter for " + movie4 + ": " + tomatoMeter4);
		console.log("5. " + movie5);
		console.log("Tomato Meter for " + movie5 + ": " + tomatoMeter5);
	};
	movies();


/*	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.*/


//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends()

/*console.log(friend1);
console.log(friend2);
console.log(friend3);*/